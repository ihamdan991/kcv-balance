# KCV BALANCE

This Project is used to check user's balance

In cryptography, a Key Checksum Value (KCV) is the checksum of a cryptographic key. It is used to validate the key integrity or compare keys without knowing their actual values. The KCV is computed by encrypting a block of bytes, each with value '00' or '01', with the cryptographic key and retaining the first 6 hexadecimal characters of the encrypted result. It is used in key management in different ciphering devices, like SIM-cards or Hardware Security Modules (HSM).

In this project, I utilize the Sha 512 encryption algorithm as a checking and security system for user balances recorded in the database. So if an attacker can access and edit the database, the value of the balance will be invalid, and the balance cannot be used.

### Repository Project 
https://gitlab.com/ihamdan991/kcv-balance


### Prerequisites

What things you need to install the software and how to install them

```
Node.js v.14.x or higher
Node Package Manager
```

### Installing

A step by step series of examples that tell you have to get a development env running

Say what the step will be
- Create ENV file (.env) with this configuration:
```
PORT=
DB_HOST=
DB_USER=
DB_PASS=
DB_NAME=
BALANCE_KEY=
API_KEY=
...
...
```
- Then run this command
```
$ npm install
$ npm start
```


### Built With

* [expressJs] The rest framework used
* [Npm] - Dependency Management