const express = require('express');
const router = express.Router();
const domain = require('./domain')
const validatePayload = require('../../helpers/schema')

router.post('/transfer', async (req, res, next) => {
    const validate = validatePayload.validate(req.body, validatePayload.balanceTransfer)
    if (!validate.err) {
        const transfer = await domain.transfer(validate.data)
        res.status(transfer.status).send(transfer.result)
    } else {
        res.status(400).send(validate.data)
    }
})

module.exports = router;