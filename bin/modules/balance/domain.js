const query = require('./queries')
const helper = require('../../helpers/helper')
const wrapper = require('../../helpers/utils/wrapper')
const config = require('../../config/globalConfigs')

const transfer = async payload => {
    const getUsers = await Promise.all([
        query.getUser(payload.applicantId),
        query.getUser(payload.beneficiaryId)
    ])

    if (getUsers[0].err || getUsers[1].err) {
        return wrapper.responseCode(500)
    }

    let users = {}
    getUsers.map(x => {
        data = x.result[0]
        users[data.wallet_id] = {
            name: data.name,
            value: data.balance,
            valid: helper.checkKcv(`${data.trx_id}${data.wallet_id}${data.balance}`, config.get('/balanceKey'), data.kcv)
        }
    })


    //check wallet
    if (!users[payload.applicantId]) {
        return wrapper.responseCode(400, { reason: "invalid applicantId" })
    } else if (!users[payload.beneficiaryId]) {
        return wrapper.responseCode(400, { reason: "invalid beneficiaryId" })
    } else if (payload.applicantId == payload.beneficiaryId) {
        return wrapper.responseCode(406, { reason: "applicantId and beneficiaryId can't be same" })
    } else if (users[payload.applicantId].value < payload.amount) {
        return wrapper.responseCode(406, { reason: "transfer amount exceeds applicantId balance" })
    }


    //validate kcv wallet
    if (!users[payload.applicantId].valid) {
        return wrapper.responseCode(406, { reason: "invalid applicantId kcv" })
    } else if (!users[payload.beneficiaryId].valid) {
        return wrapper.responseCode(406, { reason: "invalid beneficiaryId kcv" })
    }

    const sendBalance = await query.sendBalance(payload.transactionId,
        payload.applicantId,
        users[payload.applicantId].value,
        payload.beneficiaryId,
        users[payload.beneficiaryId].value,
        payload.amount
    )
    if (sendBalance.err) {
        return wrapper.responseCode(500)
    }

    const applicant = {
        id: sendBalance.result[0][0].wallet_id,
        balance: sendBalance.result[0][0].balance,
        kcv: helper.sha512(`${sendBalance.result[0][0].applicantTrxId}${sendBalance.result[0][0].wallet_id}${sendBalance.result[0][0].balance}`, config.get('/balanceKey'))
    }
    const beneficiary = {
        id: sendBalance.result[1][0].wallet_id,
        balance: sendBalance.result[1][0].balance,
        kcv: helper.sha512(`${sendBalance.result[1][0].beneficiaryTrxId}${sendBalance.result[1][0].wallet_id}${sendBalance.result[1][0].balance}`, config.get('/balanceKey'))
    }
    const updateKcv = await query.updateKcv(applicant, beneficiary)
    if (updateKcv.err) {
        return wrapper.responseCode(500)
    }

    return wrapper.responseCode(0, {
        applicant: {
            name: users[payload.applicantId].name,
            intialBalance: users[payload.applicantId].value,
            lastBalance: applicant.balance
        },
        beneficiary: {
            name: users[payload.beneficiaryId].name,
            intialBalance: users[payload.beneficiaryId].value,
            lastBalance: beneficiary.balance
        }
    })
}


module.exports = {
    transfer
}