const { connection } = require('../../helpers/databases/mysql')

const getUser = async walletId => {
    const result = await connection
        .promise()
        .query(`SELECT A.*, B.id AS trx_id FROM users A
            LEFT JOIN transaction_history B ON A.wallet_id=B.wallet_id
            WHERE A.wallet_id=? ORDER BY B.id DESC LIMIT 1`, [walletId])
        .then(([rows, fields]) => {
            return { err: false, result: rows }
        }).catch(e => {
            console.log(e);
            return { err: true, result: false }
        })
    return result
}

const sendBalance = async (transactionId, applicantId, applicantBalance, beneficiaryId, beneficiaryBalance, amount) => {
    const result = await connection
        .promise()
        .query(`CALL sendBalance(?,?,?,?,?,?)`, [transactionId, applicantId, applicantBalance, beneficiaryId, beneficiaryBalance, amount])
        .then(([rows, fields]) => {
            return { err: false, result: rows }
        }).catch(e => {
            console.log(e);
            return { err: true, result: false }
        })
    return result
}

const updateKcv = async (applicant, beneficiary) => {
    const result = await connection
        .promise()
        .query(`CALL updateKcv(?,?,?,?,?,?)`,
            [applicant.id, applicant.balance, applicant.kcv, beneficiary.id, beneficiary.balance, beneficiary.kcv])
        .then(([rows, fields]) => {
            return { err: false, result: rows }
        }).catch(e => {
            console.log(e);
            return { err: true, result: false }
        })
    return result
}

module.exports = {
    getUser,
    sendBalance,
    updateKcv
}