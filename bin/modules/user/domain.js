const query = require('./queries')
const helper = require('../../helpers/helper')
const wrapper = require('../../helpers/utils/wrapper')
const config = require('../../config/globalConfigs')

const users = async userId => {
    const getUsers = await query.getUsers(userId)
    if (getUsers.err) {
        return wrapper.responseCode(500)
    } else if (getUsers.result.length === 0) {
        return wrapper.responseCode(404)
    }

    const users = getUsers.result.map(x => {
        return {
            wallet_id: x.wallet_id,
            name: x.name,
            balance: {
                value: x.balance,
                valid: helper.checkKcv(`${x.trx_id}${x.wallet_id}${x.balance}`, config.get('/balanceKey'), x.kcv)
            },
            created_at: x.created_at,
            updated_at: x.updated_at
        }
    })

    let result = {}
    if (!userId) {
        result = {
            length: users.length,
            result: users
        }
    } else {
        result = users[0]
    }

    return wrapper.responseCode(0, result)
}

module.exports = {
    users
}