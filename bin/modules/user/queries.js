const { connection } = require('../../helpers/databases/mysql')

const getUsers = async userId => {
    const conditions = userId ? `WHERE A.wallet_id=${userId}` : ''
    const result = await connection
        .promise()
        .query(`SELECT A.*, B.id AS trx_id
            FROM users A
            LEFT JOIN transaction_history B ON A.wallet_iD=B.wallet_id
            ${conditions} ORDER BY B.id DESC`)
        .then(([rows, fields]) => {
            return { err: false, result: rows }
        }).catch(e => {
            console.log(e);
            return { err: true, result: false }
        })
    return result
}

module.exports = {
    getUsers
}