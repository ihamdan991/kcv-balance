const express = require('express');
const router = express.Router();
const domain = require('./domain')
const validatePayload = require('../../helpers/schema')

router.get('/', async (req, res, next) => {
    const users = await domain.users()
    res.status(users.status).send(users.result)
})

router.get('/:id', async (req, res, next) => {
    const validate = validatePayload.validate(req.params, validatePayload.user)
    if (!validate.err) {
        const user = await domain.users(validate.data.id)
        res.status(user.status).send(user.result)
    } else {
        res.status(400).send(validate.data)
    }
})

module.exports = router;