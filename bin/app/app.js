const express = require('express')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const wrapper = require('../helpers/utils/wrapper')
const apiKey = require('../auth/api_key').init

const app = express()
app.use(logger('short'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())

app.use((err, req, res, next) => {
   if (err instanceof SyntaxError && err.status === 400 && 'body' in err) {
      return res.status(400).send(wrapper.responseCode(400, { reason: 'Invalid Body' }).result)
   }
   next()
})

const userRoutes = require('../modules/user/user')
const balanceRoutes = require('../modules/balance/balance')

app.use('/api/balance', apiKey, balanceRoutes)
app.use('/api/users', apiKey, userRoutes)
app.use((req, res, next) => {
   res.status(404).send(wrapper.responseCode(404).result)
})

module.exports = app