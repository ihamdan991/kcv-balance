require('dotenv').config();
const confidence = require('confidence');

const config = {
    mysqlConnection: {
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        password: process.env.DB_PASS,
        database: process.env.DB_NAME,
        connectionLimit: 100,
        debug: false,
        dateStrings: true
    },
    balanceKey: process.env.BALANCE_KEY,
    apiKey: process.env.API_KEY,
}

const store = new confidence.Store(config);

exports.get = key => store.get(key);