const wrapper = require('../helpers/utils/wrapper')
const globalConfig = require('../config/globalConfigs')

const init = async (req, res, next) => {
    const checkAuth = await checkAuthApiKey(req.headers)
    if (checkAuth.err) {
        return res.status(401).json(wrapper.responseCode(401, checkAuth.result).result)
    }
    req.tmoney = checkAuth.result
    next()
}

const checkAuthApiKey = async param => {
    const getApiKey = param['x-api-key']
    if (!getApiKey) {
        return { err: true }
    } else if (getApiKey !== globalConfig.get('/apiKey')) {
        return { err: true }
    }
    return { err: false, result: true }
}

module.exports = {
    init
}
