const Joi = require('joi')
const validateJoi = require('validate.js');
const wrapper = require('./utils/wrapper')

const validate = (param, schema) => {
    const { value, error } = Joi.validate(param, schema)
    if (!validateJoi.isEmpty(error)) {
        return {
            err: true,
            data: wrapper.responseCode(400, { reason: error.details[0].message }).result
        }
    }
    return { err: false, data: value }
}

const user = Joi.object().keys({
    id: Joi.string().required(),
})


const balanceTransfer = Joi.object().keys({
    applicantId: Joi.string().required(),
    beneficiaryId: Joi.string().required(),
    amount: Joi.number().required(),
    transactionId: Joi.string().required(),
})


module.exports = {
    validate,
    user,
    balanceTransfer
}