const moment = require('moment')

const dateNow = () => {
    return moment().format('YYYY-MM-DD HH:mm:ss')
}

const response = (status, code, msg, result) => {
    return {
        status: status,
        result: {
            resultCode: code,
            resultDesc: msg,
            content: result,
            timeStamp: dateNow()
        }
    }
}

const responseCode = (code, value) => {
    switch (code) {
        case 0:
            return response(200, '0', 'SUKSES & di-approve oleh sistem', value ? value : {})
            break;
        case 400:
            return response(400, '400', 'Bad Request', value ? value : {})
            break;
        case 403:
            return response(403, '403', 'Forbidden', value ? value : {})
            break;
        case 404:
            return response(404, '404', 'Not Found', value ? value : {})
            break;
        case 401:
            return response(401, '401', 'UnauthorizedError', value ? value : {})
            break;
        case 406:
            return response(406, '406', 'Not Acceptable', value ? value : {})
            break;
        case 503:
            return response(503, '503', 'Service Unavailable', value ? value : {})
            break;
        default:
            return response(500, '500', 'Internal Server Error', value ? value : {})

    }
}

const data = (code, msg, value) => {
    return response(200, code, msg, value)
}

module.exports = {
    response,
    responseCode,
    data
}