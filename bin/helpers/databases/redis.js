const redis = require('redis')
const { promisifyAll } = require('bluebird')
const globalConfig = require('../../config/globalConfigs')

promisifyAll(redis)

const client = redis.createClient(globalConfig.get('/redisConnection'))
client.on('error', err => {
    console.log('Error redis : ', err)
})

const set = async (key, value, expire) => {
    const result = await client.setAsync(key, value)
    client.expire(key, expire)
    return result
}

const get = async key => {
    const result = await client.getAsync(key)
    return result
}

module.exports = {
    set,
    get
}