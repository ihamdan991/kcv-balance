const moment = require('moment')
const crypto = require("crypto")
const { v1: uuid } = require('uuid')

const dateNow = () => {
    return moment().format('YYYY-MM-DD HH:mm:ss')
}

const sha512 = (string, sign) => {
    var hash = crypto.createHmac('sha512', sign)
    hash.update(string)
    return hash.digest('hex')
}

const checkKcv = (string, key, kcv) => {
    console.log(string, sha512(string, key));
    return sha512(string, key) === kcv
}

module.exports = {
    uuid,
    dateNow,
    sha512,
    checkKcv
}