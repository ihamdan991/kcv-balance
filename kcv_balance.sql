-- Adminer 4.8.1 MySQL 5.5.5-10.8.3-MariaDB-1:10.8.3+maria~jammy dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP DATABASE IF EXISTS `kcv_balance`;
CREATE DATABASE `kcv_balance` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `kcv_balance`;

DELIMITER ;;

CREATE PROCEDURE `sendBalance`(IN `transactionId` varchar(50), IN `applicantId` varchar(20), IN `applicantBalance` double(16,2), IN `beneficiaryId` varchar(20), IN `beneficiaryBalance` double(16,2), IN `amount` double(16,2))
BEGIN

DECLARE exit handler for sqlexception
    BEGIN
    ROLLBACK;
END;

DECLARE exit handler for sqlwarning
    BEGIN
    ROLLBACK;
END;

START TRANSACTION;
INSERT INTO transaction_history 
(wallet_id, transaction_id, initial_balance, debit_balance, credit_balance, last_balance) 
VALUES(applicantId, transactionId, applicantBalance, amount, 0, applicantBalance-amount);

SELECT LAST_INSERT_ID() as applicantTrxId, applicantBalance-amount as balance, applicantId as wallet_id;

INSERT INTO transaction_history 
(wallet_id, transaction_id, initial_balance, debit_balance, credit_balance, last_balance) 
VALUES(beneficiaryId, transactionId, beneficiaryBalance, 0, amount, beneficiaryBalance+amount);

SELECT LAST_INSERT_ID() as beneficiaryTrxId, beneficiaryBalance+amount as balance, beneficiaryId as wallet_id;

COMMIT;

END;;

CREATE PROCEDURE `updateKcv`(IN `applicantId` varchar(20), IN `applicantBalance` double(16,2), IN `applicantKcv` varchar(255), IN `beneficiaryId` varchar(255), IN `beneficiaryBalance` double(16,2), IN `beneficiaryKcv` varchar(255))
BEGIN

DECLARE exit handler for sqlexception
    BEGIN
    ROLLBACK;
END;

DECLARE exit handler for sqlwarning
    BEGIN
    ROLLBACK;
END;

START TRANSACTION;

UPDATE users SET balance=applicantBalance, kcv=applicantKcv, updated_at=NOW() WHERE wallet_id=applicantId;
UPDATE users SET balance=beneficiaryBalance, kcv=beneficiaryKcv, updated_at=NOW() WHERE wallet_id=beneficiaryId;

COMMIT;

END;;

DELIMITER ;

DROP TABLE IF EXISTS `transaction_history`;
CREATE TABLE `transaction_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `wallet_id` varchar(20) NOT NULL,
  `transaction_id` varchar(50) NOT NULL,
  `transaction_time` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `initial_balance` double(16,2) NOT NULL DEFAULT 0.00,
  `debit_balance` double(16,2) NOT NULL DEFAULT 0.00,
  `credit_balance` double(16,2) NOT NULL DEFAULT 0.00,
  `last_balance` double(16,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `wallet_id` (`wallet_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

TRUNCATE `transaction_history`;
INSERT INTO `transaction_history` (`id`, `wallet_id`, `transaction_id`, `transaction_time`, `initial_balance`, `debit_balance`, `credit_balance`, `last_balance`) VALUES
(1,	'08112444925',	'65b9a380-6f13-11ed-a504-41cfed10231c',	'2022-11-20 18:30:50',	0.00,	0.00,	1000000.00,	1000000.00);

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `wallet_id` varchar(20) NOT NULL,
  `name` varchar(30) NOT NULL,
  `balance` double(16,2) NOT NULL,
  `kcv` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `wallet_id` (`wallet_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

TRUNCATE `users`;
INSERT INTO `users` (`id`, `wallet_id`, `name`, `balance`, `kcv`, `created_at`, `updated_at`) VALUES
(1,	'08112444925',	'Hamdan',	10000000.00,	'd34cd8da0db176626628c5667f277b9d29068f25db0c8668369f095807838f084a869377443a6bf853db2464eba35aedbcdd25e969d3924570fccb94a2d99c9a',	'2022-11-20 17:50:46',	NULL),
(2,	'08112444924',	'Ibrahim',	0.00,	'c718764550e79424c2d97a3f9116b0c3c173910880ea74f16f2208de6412dc0a025e5cedf277d6e4e4310d0292a5e4085ad10e8d5987ecf4803a71428dce61ce',	'2022-11-20 17:51:38',	NULL),
(3,	'08112444922',	'Khalid',	0.00,	'39c3a1f0a4f661399847f766901881deab4a374510f1f56710489cdaff0fec9ada1979ade161eea27c8dbba312bfe75c75b3ef93e0178bfebcb7a16ef3074bc2',	'2022-11-20 17:51:50',	NULL);

-- 2022-11-21 05:33:56
